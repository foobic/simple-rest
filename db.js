const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const fs = require("fs");
const { COPYFILE_EXCL } = fs.constants;

const adapter = new FileSync("db.json");
const db = low(adapter);

module.exports = db;
