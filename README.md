# Simple REST app

Just simple REST application written on Expressjs.

## Installation

```js
npm install
```

## Start

```js
npm start
```
then, check `http://localhost:3000`

## Testing

```js
npm test
```

or if you want to get also code coverage report then you should use:

```js
npm test-cov
```

## Documentation

Documentation available at `http://localhost:3000/docs`

## Other

Be carefull that `npm test` and `npm test-cov` brokes `db.json`, so, don't forget to `git checkout` it.
