const swaggerJsdoc = require("swagger-jsdoc");

const options = {
  swaggerDefinition: {
    info: {
      title: "Simple REST app",
      version: "1.0.0"
    }
  },
  apis: ["./routes/*.js"],
  explorer: true
};

const specs = swaggerJsdoc(options);

module.exports = specs;
