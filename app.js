const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const favicon = require("serve-favicon");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const swaggerSpecs = require("./swagger.docs");

const app = express();

app.use(
  "/docs",
  swaggerUi.serve,
  swaggerUi.setup(swaggerSpecs, true, { supportedSubmitMethods: ["get"] })
);
app.use(cors());
app.use(favicon(__dirname + "/assets/favicon.ico"));
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/", indexRouter);
app.use("/users", usersRouter);

module.exports = app;
