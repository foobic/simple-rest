const userRepo = require("../repositories/user.repository");
const Joi = require("joi");

const userScheme = {
  name: Joi.string()
    .alphanum()
    .min(2)
    .max(30)
    .required(),
  health: Joi.number()
    .integer()
    .min(0)
    .max(100)
    .required(),
  attack: Joi.number()
    .integer()
    .required(),
  defense: Joi.number()
    .integer()
    .required(),
  source: Joi.string()
    .uri()
    .required()
};

const userService = {
  getAll: () => {
    return userRepo.getAll();
  },
  getOneById: id => {
    return userRepo.getOneById(parseInt(id));
  },
  createOne: user => {
    return userRepo.createOne(user);
  },
  updateOne: (id, newUser) => {
    return userRepo.updateOne(parseInt(id), newUser);
  },
  deleteOne: id => {
    return userRepo.deleteOne(parseInt(id));
  },
  validate: object => {
    const { error } = Joi.validate(object, userScheme, { convert: false });
    if (error) return { passed: false, err: error.message };
    return { passed: true, user: object };
  }
};

module.exports = userService;
