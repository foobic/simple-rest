const db = require("../db");

const userRepo = {
  getAll: () => {
    return db.get("users").value();
  },
  createOne: user => {
    const maxId = db
      .get("users")
      .sortBy("_id")
      .reverse()
      .take(1)
      .value()[0]._id;
    user._id = maxId + 1;
    db.get("users")
      .push(user)
      .write();
    return userRepo.getOneById(user._id);
  },
  updateOne: (id, newUser) => {
    const user = userRepo.getOneById(id);
    if (!user) return false;
    const res = db
      .get("users")
      .find({ _id: id })
      .assign(newUser)
      .write();
    return userRepo.getOneById(id);
  },
  deleteOne: id => {
    const user = userRepo.getOneById(id);
    if (!user) return false;
    db.get("users")
      .remove({ _id: id })
      .write();
    return user;
  },
  getOneById: id => {
    return db
      .get("users")
      .find({ _id: id })
      .value();
  }
};
module.exports = userRepo;
