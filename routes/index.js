const express = require("express");
const router = express.Router();
const util = require("util");
const request = util.promisify(require("request"));
const marked = require("marked");

const readmeUrl = `https://bitbucket.org/foobic/simple-rest/raw/master/README.md`;

/**
 * @swagger
 *
 * /:
 *    get:
 *      description: Autofetch and display README.md from repo in Markdown format.
 */
router.get("/", function(req, res, next) {
  try {
    request(readmeUrl).then(result => {
      const { statusCode, body: readme } = result;

      if (statusCode != 200)
        throw new Error(`Readme not found at ${readmeUrl}`);
      res.status(200).send(marked(readme));
    });
  } catch (e) {
    res.status(404).send(e.message);
  }
});

module.exports = router;
