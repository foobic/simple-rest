const express = require("express");
const router = express.Router();

const userService = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

/**
 * @swagger
 *
 * /users:
 *    get:
 *      description: Get an array of all users
 *      responses:
 *         200:
 *            description: List of all users
 */
router.get("/", function(req, res, next) {
  try {
    const users = userService.getAll();
    console.log(users);
    if (users) return res.status(200).json(users);
    return res.status(404).end();
  } catch (e) {
    console.log(e);
    res.status(500).end();
  }
});

/**
 * @swagger
 *
 * /users/:id:
 *    get:
 *      description: Get user by ID
 */
router.get("/:id", function(req, res, next) {
  try {
    const user = userService.getOneById(req.params.id);
    if (user) return res.status(200).json(user);
    return res.status(404).end();
  } catch (e) {
    console.log(e);
    res.status(500).end();
  }
});

/**
 * @swagger
 *
 * /users:
 *    post:
 *      description: Create new user
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authorization
 *         description: Authorization header
 *         in: header
 *         required: true
 *         type: string
 *       - name: name
 *         description: Name
 *         in: formData
 *         required: true
 *         type: string
 *       - name: health
 *         description: Health
 *         in: formData
 *         required: true
 *         type: number
 *       - name: attack
 *         description: Attack
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: defense
 *         description: Defense
 *         in: formData
 *         required: true
 *         type: integer
 *      responses:
 *         '200':
 *            description: Creates one user
 *         '400':
 *            description: Wrong data was sent
 */
router.post("/", isAuthorized, function(req, res, next) {
  try {
    const validation = userService.validate(req.body);
    if (validation.passed) {
      const createdUser = userService.createOne(validation.user);
      return res.status(200).json(createdUser);
    }
    return res.status(400).send(validation.err);
  } catch (e) {
    console.log(e);
    res.status(500).end();
  }
});

/**
 * @swagger
 *
 * /users:
 *    put:
 *      description: Update existing user
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authorization
 *         description: Authorization header
 *         in: header
 *         required: true
 *         type: string
 *       - name: name
 *         description: Name
 *         in: formData
 *         required: true
 *         type: string
 *       - name: health
 *         description: Health
 *         in: formData
 *         required: true
 *         type: number
 *       - name: attack
 *         description: Attack
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: defense
 *         description: Defense
 *         in: formData
 *         required: true
 *         type: integer
 *      responses:
 *         '200':
 *            description: Existing user successfully updated
 *         '400':
 *            description: Wrong data was sent
 */
router.put("/:id", isAuthorized, function(req, res, next) {
  try {
    const validation = userService.validate(req.body);
    if (validation.passed) {
      const updatedUser = userService.updateOne(req.params.id, validation.user);
      if (updatedUser) return res.status(200).json(updatedUser);
      return res.status(400).send(`Bad request`);
    }
    return res.status(400).send(validation.err);
  } catch (e) {
    console.log(e);
    res.status(500).end();
  }
});

/**
 * @swagger
 *
 * /users/:id:
 *    delete:
 *      description: Delete user by id
 *      produces:
 *       - application/json
 *      responses:
 *         '200':
 *            description: User deleted
 *         '400':
 *            description: Wrong data was sent
 */
router.delete("/:id", isAuthorized, function(req, res, next) {
  try {
    const deletedUser = userService.deleteOne(req.params.id);
    if (!deletedUser)
      return res.status(400).send("User with this id does not exist");
    return res
      .status(200)
      .send(`User ${JSON.stringify(deletedUser)} successfully deleted.`);
  } catch (e) {
    console.log(e);
    res.status(500).end();
  }
});

module.exports = router;
