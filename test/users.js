process.env.NODE_ENV = "testing";

let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../app");
let should = chai.should();
const fs = require("fs");

chai.use(chaiHttp);

describe("Users", () => {
  describe("GET", () => {
    it("it should return all users", done => {
      chai
        .request(server)
        .get("/users")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          done();
        });
    });
    it("it should return user with _id = 1", done => {
      chai
        .request(server)
        .get("/users/1")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
    it("it should return 404 err", done => {
      chai
        .request(server)
        .get("/users/9999999")
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });
  describe("POST", () => {
    it("it should create new user", done => {
      chai
        .request(server)
        .post("/users")
        .send({
          name: "12",
          health: 4,
          attack: 4,
          defense: 3,
          source: "https://expressjs.com/images/express-facebook-share.png"
        })
        .set("Authorization", "admin")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
    it("it should return validation err", done => {
      chai
        .request(server)
        .post("/users")
        .send({
          name: 12,
          health: 4,
          attack: 4,
          defense: 3,
          source: "https://expressjs.com/images/express-facebook-share.png"
        })
        .set("Authorization", "admin")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    it("it should failed because of authorization err", done => {
      chai
        .request(server)
        .post("/users")
        .send({
          name: "12",
          health: 4,
          attack: 4,
          defense: 3,
          source: "https://expressjs.com/images/express-facebook-share.png"
        })
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });
  describe("PUT", () => {
    it("it should return validation err", done => {
      chai
        .request(server)
        .put("/users/1")
        .send({
          name: "kek",
          health: 400,
          attack: 4,
          defense: 3,
          source: "https://expressjs.com/images/express-facebook-share.png"
        })
        .set("Authorization", "admin")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    it("it should successfully update user", done => {
      chai
        .request(server)
        .put("/users/1")
        .send({
          name: "kek",
          health: 40,
          attack: 4,
          defense: 3,
          source: "https://expressjs.com/images/express-facebook-share.png"
        })
        .set("Authorization", "admin")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });

    it("it should not update user because of wrong id", done => {
      chai
        .request(server)
        .put("/users/99999999")
        .send({
          name: "kek",
          health: 40,
          attack: 4,
          defense: 3,
          source: "https://expressjs.com/images/express-facebook-share.png"
        })
        .set("Authorization", "admin")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
  describe("DELETE", () => {
    it("it should successfully delete user with _id=1", done => {
      chai
        .request(server)
        .delete("/users/1")
        .set("Authorization", "admin")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it("it should return 400 err - user with this id does not exists", done => {
      chai
        .request(server)
        .delete("/users/9999999")
        .set("Authorization", "admin")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it("it should return 401 err - Authorization err", done => {
      chai
        .request(server)
        .delete("/users/9999999")
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });
});
