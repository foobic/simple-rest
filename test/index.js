process.env.NODE_ENV = "testing";

let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../app");
let should = chai.should();
const fs = require("fs");

chai.use(chaiHttp);
describe("/", () => {
  describe("GET", () => {
    it("it should just return 200", done => {
      chai
        .request(server)
        .get("/")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
});
